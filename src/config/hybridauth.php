<?php
return array(

     
    'providers' => array (

        "Google" => array (
            "enabled" => false,
			"icon"=>'fa-google',
            "keys"    => array ( "id" => "", "secret" => ""  ),
            "scope"   => "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email" // optional
        ),

        'Facebook' => array (
            'enabled' => true,
			"icon"=>'fa-facebook-square',
            'keys'    => array ( 'id' => '', 'secret' => '' ),
            "scope"   => "email", // optional
        ),

        'Twitter' => array (
            'enabled' => true,
			"icon"=>'fa-twitter-square',
            'keys'    => array ( 'key' => '', 'secret' => '' )
        ),
        'Instagram' => array (
            'enabled' => false,
			"icon"=>'fa-instagram',
            'keys'    => array ( 'key' => '', 'secret' => '' )
        ),
        'OpenID' => array (
            'enabled' => false,
			"icon"=>'fa-openid', 
			 
        ),
        'Steam' => array (
            'enabled' => true,
			"icon"=>'fa-steam', 
			"wrapper" => array( 
				'class'=>'Hybrid_Providers_Steam',
				'path' => base_path().'/vendor/hybridauth/hybridauth/additional-providers/hybridauth-steam/Providers/Steam.php'
			)
        ),
        // 'Github' => array (
            // 'enabled' => true,
			// "icon"=>'fa-twitter-square', 
			
        // ),

        // 'LinkedIn' => array (
            // 'enabled' => true,
            // 'keys'    => array ( 'key' => '', 'secret' => '' )
        // ),
    )







);