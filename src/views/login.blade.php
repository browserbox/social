<div class="social-logins">

@foreach( Config::get('social::hybridauth.providers') as $provider_name=>$settings ) 
	@if( $settings['enabled'] )
	<a class="nojax"  href="{{ URL::route('social.login',$provider_name) }}"><i class="fa {{ $settings['icon'] }}"></i></a>
	@endif 
@endforeach
</div> 