@extends('layouts.scaffold')
@section('pagetitle')
{{ trans('projects.all') }}
@stop
@section('main')

{{ Form::open(array('route' => 'social.store', 'class' => 'form ', 'id'=>"new-user-form")) }}
<div class="broserbox-social ">
  <h2>{{ trans('Social::social.new_registration')}}</h2>
  <p>{{ trans('Social::social.complete_registration')}}</p>
	<div class="row  ">
		<div class="col-sm-6  ">
			<input type="hidden" name="provider" value="{{ $provider }}" />
		  
		 
				 <div class="form-group {{ ($errors->has('first_name')) ? 'has-error' : '' }}">
					{{ Form::label('first_name',trans('Social::social.first_name') ) }}
					{{ Form::text('first_name', null, array('class' => 'form-control', 'placeholder' => '', 'required'=>'true')) }}
					{{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}
				</div>
				 <div class="form-group {{ ($errors->has('last_name')) ? 'has-error' : '' }}">
					{{ Form::label('last_name',trans('Social::social.last_name') ) }}
					{{ Form::text('last_name', null, array('class' => 'form-control', 'placeholder' => '', 'required'=>'true')) }}
					{{ ($errors->has('last_name') ? $errors->first('last_name') : '') }}
				</div>
				 <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
					{{ Form::label('email',trans('Social::social.email') ) }}
					{{ Form::email('email', null, array('class' => 'form-control', 'placeholder' => '', 'required'=>'true')) }}
					{{ ($errors->has('email') ? $errors->first('email') : '') }}
				</div>
			 
			

		<button type="submit" class="btn btn-primary ">Gravar</button>
		</div>
	</div>
</div>
{{ Form::close() }}
@stop
