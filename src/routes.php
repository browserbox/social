<?php
Route::group(array('prefix' => 'todosqueremosumbairromelhor'), function() {
	Route::get('social/login/{provider}',array('as'=>'social.login','uses'=>'\Browserbox\Social\AuthController@login'));
	Route::get('social/endpoint',array('as'=>'social.endpoint','uses'=>'\Browserbox\Social\AuthController@endpoint'));
	Route::get('social/logout',array('as'=>'social.logout','uses'=>'\Browserbox\Social\AuthController@logout'));
	Route::get('social/registration',array('as'=>'social.registration','uses'=>'\Browserbox\Social\AuthController@registration'));
	Route::post('social/store',array('as'=>'social.store','uses'=>'\Browserbox\Social\AuthController@store'));
	Route::post('social/token',array('as'=>'social.token','uses'=>'\Browserbox\Social\AuthController@token'));
});
 