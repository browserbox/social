<?php namespace Browserbox\Social;

use Hybrid_Auth;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router; 

class SocialServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('browserbox/social');
		 
		require_once(__DIR__ . './../../routes.php');
		
		
		// Add the Views Namespace 
		if (is_dir(app_path().'/views/packages/browserbox/social'))
		{
			// The package views have been published - use those views. 
			$this->app['view']->addNamespace('Social', array(app_path().'/views/packages/browserbox/social'));
		}
		else 
		{
			// The package views have not been published. Use the defaults. 
			$this->app['view']->addNamespace('Social' ,  __DIR__ .'/../../views'); 
			 
		}
		
		 
		  if (is_dir(app_path().'/views/packages/browserbox/social'))
		{
			// The package views have been published - use those views. 
			\Lang::addNamespace('Social', array(app_path().'/lang/packages/browserbox/social'));
		}
		else 
		{
			// The package views have not been published. Use the defaults. 
			\Lang::addNamespace('Social' ,  __DIR__ .'/../../lang'); 
			 
		}  
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		 $this->app['hybridauth'] = $this->app->share(function($app) {
            $config = $app['config'];
            $haconfig = $config['social::hybridauth'];
            $haconfig['base_url'] = $app['url']->route('social.endpoint');
            $instance = new Hybrid_Auth($haconfig);
            return $instance;
        });
		
		// private function registerAnvard() {
        $this->app['social'] = $this->app->share(function($app) {
            $config = array(
                'db' => $app['config']['social::db'],
                'hybridauth' => $app['config']['social::hybridauth'],
                'models' => $app['config']['social::models'],
                'routes' => $app['config']['social::routes'],
            );
		  
            $instance = new Social($config,$app['events']);
            $instance->setLogger($app['log']);
			
            return $instance;
        });
		
	
		
		// Add the Views Namespace 
		
		
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('social', 'hybridauth');
	}

}
