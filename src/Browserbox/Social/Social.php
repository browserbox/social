<?php namespace Browserbox\Social;

use App;
use \Redirect;
use \Input;
use Sentry;
use Hybrid_Auth;
use Hybrid_Provider_Adapter;
use Hybrid_User_Profile;
use Illuminate\Log\Writer;
use Illuminate\Config\Repository;
use Illuminate\Events\Dispatcher;
use \Sentinel\Repo\User;

class Social {

    /**
     * The configuration for Social
     *
     * @var array
     */
    protected $config;
    protected $dispatcher;

    /**
     * The service used to login
     *
     * @var Hybrid_Provider_Adapter $adapter
     */
    protected $adapter;


    /**
     * The profile of the current user from
     * the provider, once logged in
     *
     * @var Hybrid_User_Profile
     */
    protected $adapter_profile;

    /**
     * The name of the current provider,
     * e.g. Facebook, LinkedIn, etc
     *
     * @var string
     */
    protected $provider;

    /**
     * The logger
     *
     * @var Writer
     */
    protected $logger;

    public function __construct(  $config  ,Dispatcher $dispatcher) {
        $this->config = $config;
		 
        $this->dispatcher = $dispatcher;
		
    }

    /**
     * Get a social profile for a user, optionally specifying
     * which social network to get, and which user to query
     */
    public function getProfile($network = NULL, $user = NULL) {
        if ( $user === NULL ) {
            $user = Auth::user();
            if (!$user) {
                return NULL;
            }
        }
        if ($network === NULL) {
            $profile = $user->profiles()->first();
        } else {
            $profile = $user->profiles()->where('network', $network)->first();
        }
        return $profile;
    }

    public function getProviders() {
        $haconfig = $this->config['hybridauth'];
        $providers = array();
        foreach ($haconfig['providers'] as $provider => $config) {
            if ( $config['enabled'] ) {
                $providers[] = $provider;
            }
        }
        return $providers;
    }

	public function logout( ) {
		$hybridAuth = App::make('hybridauth');
		// debug( $hybridAuth->getProviders() );
		$hybridAuth->logoutAllProviders();
		// die();
	}
    /**
     * @return String
     */
    public function getCurrentProvider() {
        return $this->provider;
    }
    public function setCurrentProvider(String $provider) {
        $this->provider = $provider;
    }

    /**
     * @return Hybrid_Provider_Adapter
     */
    public function getAdapter() {
        return $this->adapter;
    }
    public function setAdapter(Hybrid_Provider_Adapter $adapter) {
        $this->adapter = $adapter;
    }


    /**
     * @return Writer
     */
    public function getLogger() {
        return $this->logger;
    }
    public function setLogger(Writer $logger) {
        $this->logger = $logger;
    }

    /**
     * Attempt a login with a given provider
     */
    public function attemptAuthentication($provider, Hybrid_Auth $hybridauth, $retry = 0,$email='') {
        try {
            $this->provider = $provider;
            $adapter = $hybridauth->authenticate($provider);
            $this->setAdapter($adapter);
            $this->setAdapterProfile($adapter->getUserProfile());
            $profile = $this->findProfile($email);
            return $profile;
        } catch (\Exception $e) {
            // uncomment this if you want more info
            //var_dump($e->getMessage());
            $this->errorCodes($e,$provider, $hybridauth, $retry);
        }
    }

    public function setAdapterProfile(Hybrid_User_Profile $profile) {
        $this->adapter_profile = $profile;
    }

    /**
     * @return Hybrid_User_Profile
     */
    public function getAdapterProfile() {
        return $this->adapter_profile;
    }

    protected function findProfile($email='') {
        $adapter_profile = $this->getAdapterProfile();
        $ProfileModel = $this->config['db']['profilemodel'];
        $UserModel = $this->config['db']['usermodel'];
        $user = NULL;

        // Have they logged in with this provider before?
        $profile_builder = call_user_func_array(
            "$ProfileModel::where",
            array('provider', $this->provider)
        );
        $profile = $profile_builder
            ->where('identifier', $adapter_profile->identifier)
            ->first();

        /**
         * Check if user is currently logged in first
         *    ... then check if their is a profile matching the social user
         *    ... then check if the email matches
         * If none of the above, create a new user.
         */
        if (\Sentry::check()) {
            $user = \Sentry::getUser();
        } elseif ($profile) {
            // ok, we found an existing user
            $user = $profile->user()->first();
            $this->logger->debug('Social: found a profile, id='.$profile->id);
        } elseif ($adapter_profile->email) {
            $this->logger->debug('Social: could not find profile, looking for email');
            // ok it's a new profile ... can we find the user by email?
            $user_builder = call_user_func_array(
                "$UserModel::where",
                array('email', $adapter_profile->email)
            );
            $user = $user_builder
                ->first();
        }
        // If we haven't found a user, we need to create a new one
        if (!$user) {
            $this->logger->debug('Social: did not find user, creating');
            // $user = new $UserModel();
			 $user = \Sentry::getUserProvider()->getEmptyUser();
            // map in anything from the profile that we want in the User
            $map = $this->config['db']['profiletousermap'];
            foreach ($map as $apkey => $ukey) {
                $user->$ukey = $adapter_profile->$apkey;
            }
			 
			if( ! $adapter_profile->email && ! $email) {
				$adapter_profile->needs_completion = true;
				$response = new \stdClass;
				$response->provider =$this->provider;
				$response->needs_completion  =true;
				return $response;
			} else {
				if(  ! $adapter_profile->email &&  $email) {
					$adapter_profile->email = $email;
					$user->activated=0;
					$adapter_profile->firstName = Input::get('first_name');
					$adapter_profile->lastName = Input::get('last_name');
					foreach ($map as $apkey => $ukey) {
						$user->$ukey = $adapter_profile->$apkey;
					}
					 
					// $SentryUser = new \Sentinel\Repo\User\SentryUser;
					// $data = array();
					// $data
					// $SentryUser->store( array('
					
				} else
					$user->activated=1;
				// Default username,email,password/confirmation
				// debug($adapter_profile->email);
			
				$user->username = $adapter_profile->email;
				$user->email = $adapter_profile->email;
				$user->password = uniqid();
				
				$check = check_blacklisted($adapter_profile->email);
				if( $check != 'true') {
					$result = new stdClass;
					$result->success = false;
					$result->message = $check;
					return $result;
				}
				// $user->password_confirmation = $user->password;

				// get the custom config from the db.php config file
				$values = $this->config['db']['uservalues'];
				foreach ( $values as $key=>$value ) {
					if (is_callable($value)) {
						$user->$key = $value($user, $adapter_profile);
					} else {
						$user->$key = $value;
					}
				}
				
				$rules = $this->config['db']['userrules'];
				$result = new \stdClass;
				try {
					$result->success  = $user->save($rules);
				}
				 catch (\Cartalyst\Sentry\Users\LoginRequiredException $e)
				{
					$result->success = false;
					$result->message= trans('Sentinel::users.loginreq');
				}
				catch (\Cartalyst\Sentry\Users\UserExistsException $e)
				{
					$result->success = false;
					$result->message = trans('Sentinel::users.exists');
				}
				if ( !$result->success ) {
					$this->logger->error('Social: FAILED TO SAVE USER');
					 
					return $result;
				} else {
					// die('welcome');
					$dispatcher = new Dispatcher;
					// debug($dispatcher);
					$user = Sentry::findUserById($user->id);
					$group = Sentry::findGroupById(1);
					$user->addGroup($group);
					$this->dispatcher->fire('sentinel.user.registered', array(
						'user'      => $user,
						'activated' => $user->activated
					));
					 
				}
            }
        }
		
        if (!$profile) {
            // If we didn't find the profile, we need to create a new one
            $profile = $this->createProfileFromAdapterProfile($adapter_profile, $user);
        } else {
            // If we did find a profile, make sure we update any changes to the source
            $profile = $this->applyAdapterProfileToExistingProfile($adapter_profile, $profile);
        }
		 
        $result = $profile->save();
        if (!$result) {
			
            $this->logger->error('Social: FAILED TO SAVE PROFILE');
            return NULL;
        }
        $this->logger->info('Social: succesful login!');
        return $profile;

    }

    protected function createProfileFromAdapterProfile($adapter_profile, $user) {
        $ProfileModel = $this->config['db']['profilemodel'];
        $attributes['provider'] = $this->provider;
        // @todo use config value for foreign key name
        $attributes['user_id'] = $user->id;
        $profile = new $ProfileModel($attributes);
        $profile = $this->applyAdapterProfileToExistingProfile($adapter_profile, $profile);
        return $profile;
    }

    protected function applyAdapterProfileToExistingProfile($adapter_profile, $profile) {
        $attributes = get_object_vars($adapter_profile);
        foreach ($attributes as $k=>$v) {
            $profile->$k = $v;
        }
        return $profile;
    }

    protected function errorCodes($e, $provider, $hybridauth, $retry = 0){
        // this is straight from: http://hybridauth.sourceforge.net/userguide/Integrating_HybridAuth_SignIn.html
        // Display the recived error,
        // to know more please refer to Exceptions handling section on the userguide
        switch( $e->getCode() ){
            case 0 : \Session::flash('error', 'Unspecified error.  Code '. $e->getCode());  break;
            case 1 : \Session::flash('error', 'Hybridauth configuration error.  Code '. $e->getCode()); break;
            case 2 : \Session::flash('error', 'Provider not properly configured. Code '. $e->getCode()) ; break;
            case 3 : \Session::flash('error', 'Unknown or disabled provider. Code '. $e->getCode()) ; break;
            case 4 : \Session::flash('error', 'Missing provider application credentials. Code '. $e->getCode()); break;
            case 5 : \Session::flash('error', 'Authentification failed.
                    The user has canceled the authentication or the provider refused the connection.  Code'. $e->getCode()) ;
                break;
            case 6 : \Session::flash('error', 'User profile request failed. Most likely the user is not connected.
                to the provider and he should authenticate again.  Code '. $e->getCode());
                $hybridauth->logoutAllProviders();
                $retry++;
                break;
            case 7 : \Session::flash('error', 'User not connected to the provider.  Code '. $e->getCode());
                $hybridauth->logoutAllProviders();
                $retry++;
                break;
            case 8 : \Session::flash('error', 'Provider does not support this feature.  Code '. $e->getCode()); break;
        }

        // case 6 and 7 would need to try again, this makes it so the user doesn't have to do that manually.
        // do this a couple times
        if($retry > 0 && $retry < 3){
            $this->attemptAuthentication($provider, $hybridauth, $retry);
        }

        \Session::flash('error', '<br /><br /><b>Original error message:</b> '. $e->getMessage());
    }
}
