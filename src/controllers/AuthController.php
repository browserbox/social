<?php namespace Browserbox\Social;
use \Log;
use \App;
use \Cookie;
use \Redirect;
use \Config;
use \Session;
use \Sentry;
use \Input;
use \View;
use Hybrid_Auth;
use Hybrid_Endpoint;
use Hybrid_Provider_Adapter;
use Hybrid_User_Profile;
use Illuminate\Log\Writer;


class AuthController extends \BaseController {
	public function login($provider) {
		 Log::debug('BB Social: attempting login');
		 
                $profile = App::make('social')->attemptAuthentication($provider, App::make('hybridauth'));
				
                Log::debug('BB Social: login attempt complete');
                if ($profile) {
                    Log::debug('BB Social: login success');
                    // Auth::loginUsingId($profile->user->id);
					if ($profile->needs_completion == false) { 
						$user = Sentry::getUserProvider()->findById($profile->user->id);
						$result = $this->do_login($user);
						if( $result['success'] ) {
							
						} else {
							Cookie::queue('logout', '1',7200 );
							Session::flash('error', $result['message']);
							return Redirect::route('Sentinel\login');
						}
					} else { 
						 return Redirect::route('social.registration')->with('provider',$profile->provider) ;
					}
                } else {
                    Log::debug('BB Social: login failure');
                    Session::flash('social', 'Failed to log in!');
                }

                /**
                 * added the detection / redirection script for easier customization
                 */
				
				
                if(Session::has('redirectback')) {
                    $redirect_url = Session::get('redirectback');
                    Session::forget('redirectback');
                    return Redirect::to($redirect_url);
                }
				
                if(Session::has('login_redirect_url')) {
                    $redirect_url = Session::get('login_redirect_url');
                    Session::forget('login_redirect_url');
                    return Redirect::to($redirect_url);
                }

                /**
                 * Added default redirect location if needed.
                 */ 
                if(Config::get('social::routes.loginredirect')){
                    return Redirect::to(Config::get('social::routes.loginredirect'));
                }

                return Redirect::back();
	}
	
	private function do_login($user) {
		try {
			Sentry::login($user);
			 $result['success'] = true;
			  Cookie::queue('logout', '0',1 );
			 return $result;
		}
		catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
				Cookie::queue('logout', '1',7200 );
			    // Sometimes a user is found, however hashed credentials do
			    // not match. Therefore a user technically doesn't exist
			    // by those credentials. Check the error message returned
			    // for more information.
			    $result['success'] = false;
			    $result['message'] = trans('Sentinel::sessions.invalid');
			}
			catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e)
			{
				 Cookie::queue('logout', '1',7200 );
			    $result['success'] = false;
			    $url = route('Sentinel\resendActivationForm');
			    $result['message'] = trans('Sentinel::sessions.notactive', array('url' => $url));
			}

			// The following is only required if throttle is enabled
			catch (\Cartalyst\Sentry\Throttling\UserSuspendedException $e)
			{
				Cookie::queue('logout', '1',7200 );
			    $time = $throttle->getSuspensionTime();
			    $result['success'] = false;
			    $result['message'] = trans('Sentinel::sessions.suspended');
			}
			catch (\Cartalyst\Sentry\Throttling\UserBannedException $e)
			{
					Cookie::queue('logout', '1',7200 );
			    $result['success'] = false;
			    $result['message'] = trans('Sentinel::sessions.banned');
			}
		return $result;
	}
	
	public function registration() {
		$provider = Session::get('provider','');
		return View::make('social::registration')->with('provider',$provider);
	}
	public function store() {
		$provider = Input::get('provider','');
		// $provider = 'Twitter';
		$email = Input::get('email'); 
		if ($provider ) {
			$profile = App::make('social')->attemptAuthentication($provider, App::make('hybridauth'),0,$email)  ;
		 
			if( $profile && ( !isset($profile->success) || $profile->success == true) ) {
			 
				return Redirect::action('account_created') ;
			} else {
				Session::flash('error', $profile->message);
				return Redirect::route('social.registration')->with('provider',$provider)->withInput() ;
			}
				 
		} else {
			return Redirect::action('Sentinel\login');
		}
	}
	
	public function endpoint() {
		if(Input::get('error') == 'access_denied'){ // works with google and facebook (others untested) if the user clicks cancel on the authentication page
			$message = "Authentification failed. The user has canceled the authentication or the provider refused the connection.";
			return Redirect::to(Config::get('social::routes.authfailed'))->with('error', $message);
		}
		Hybrid_Endpoint::process();
	}
	
	public function logout() {
		$hybridAuth = App::make('hybridauth');

		// logout all providers
		$hybridAuth->logoutAllProviders();

		// logout of laravel
		Sentry::logout();
 
		// redirect someplace or back.
		if (Config::get('social::routes.logoutredirect')) {
			return Redirect::to(Config::get('social::routes.logoutredirect'));
		}
		else {
			return Redirect::back();
		}
	}
	
	public function token() {
		$hybridauth = App::make('hybridauth');
		// Set some session variables needed for HybridAuth
		switch( Input::get('type') ) {
			case 'facebook':
				Hybrid_Auth::storage()->set( 'hauth_session.facebook.is_logged_in', 1 );
				Hybrid_Auth::storage()->set( 'hauth_session.facebook.token.access_token', $_POST['fb_access_token'] );
				// $hybrid_config = require $config;
				// $fb_config     = $hybrid_config['providers']['Facebook'];
				$fb_app_id     = $_POST['fb_app_id'];
				$_SESSION['fb_'. $fb_app_id .'_access_token'] = $_POST['fb_access_token'];
				$_SESSION['fb_'. $fb_app_id .'_user_id']      = $_POST['fb_uid'];
				
				break;
			case 'cedp':
				Hybrid_Auth::storage()->set( 'hauth_session.comunidadeedp.is_logged_in', 1 );
				Hybrid_Auth::storage()->set( 'hauth_session.comunidadeedp.token.access_token', $_POST['cedp_access_token'] );
				// $hybrid_config = require $config;
				// $fb_config     = $hybrid_config['providers']['Facebook'];
				// $fb_app_id     = $_POST['fb_app_id'];
				// $_SESSION['fb_'. $fb_app_id .'_access_token'] = $_POST['fb_access_token'];
				// $_SESSION['fb_'. $fb_app_id .'_user_id']      = $_POST['fb_uid'];
				
				break;
		} 
		
	 
		// Now we connect to FB using the given access token for this user
		// $adapter      = $hybridauth->getAdapter( "facebook" );
		// $user_profile = $adapter->getUserProfile();
	}
}